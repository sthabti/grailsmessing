package g

import com.sky.floo.CropperController
import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(CropperController)
class CropperControllerSpec extends Specification {

	def setup() {
	}

	def cleanup() {
	}

	void "test something"() {
	}
}