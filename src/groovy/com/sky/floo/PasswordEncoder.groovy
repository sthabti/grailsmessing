package com.sky.floo

import org.springframework.dao.DataAccessException

class PasswordEncoder implements org.springframework.security.authentication.encoding.PasswordEncoder {

    @Override
    String encodePassword(String password, Object salt = null) throws DataAccessException {
        return password
    }

    @Override
    boolean isPasswordValid(String encodedPassword, String rawPassword, Object salt) throws DataAccessException {
        return (encodedPassword == encodePassword(rawPassword, salt))
    }
}
