import com.sky.floo.Role
import com.sky.floo.Team
import com.sky.floo.TeamRole

class BootStrap {

    def grailsApplication

    def init = { servletContext ->

        Role userRole = Role.findOrCreateWhere(authority: 'ROLE_USER')
        userRole.save(flush: true, failOnError: true)

        Role adminRole = Role.findOrCreateWhere(authority: 'ROLE_ADMIN')
        adminRole.save(flush: true, failOnError: true)

        Team admin = Team.findOrCreateWhere(email: grailsApplication.config.admin.email)
        admin.name = grailsApplication.config.admin.name
        admin.password = grailsApplication.config.admin.password
        admin.enabled = true
        admin.messagesActive = false
        admin.save(flush: true, failOnError: true)
        TeamRole.create(admin, adminRole, true)

    }

    def destroy = {
    }
}
