class UrlMappings {

    static mappings = {

        "/"(controller: 'cropper', action: 'index')
        "/show"(controller: 'cropper', action: 'show')
        "/login/$action"(controller: 'login')
        "/logout"(controller: 'logout')

        "/teams"(controller: 'team', action: 'index')
        "/teams/save"(controller: 'team', action: 'save')
        "/team/$id/delete"(controller: 'team', action: 'delete')

        "/team/$teamId/members"(controller: 'member', action: 'index')
        "/team/$teamId/member/$id"(controller: 'member', action: 'edit')
        "/team/$teamId/member/create"(controller: 'member', action: 'create')
        "/team/$teamId/member/save"(controller: 'member', action: 'save')
        "/team/$teamId/member/$id/edit"(controller: 'member', action: 'edit')
        "/team/$teamId/member/$id/update"(controller: 'member', action: 'update')
        "/team/$teamId/member/$id/delete"(controller: 'member', action: 'delete')

        "500"(view: '/error')
    }
}
