modules = {
    bootstrap {
        dependsOn 'jquery'
        resource 'css/bootstrap.css'
        resource 'js/bootstrap.js'
    }

    application {
        dependsOn 'jquery'
        resource 'css/style.css'
        resource 'js/application.js'
    }
}
