package com.sky.floo
import javax.mail.*
import javax.mail.internet.MimeMultipart
import javax.mail.search.FlagTerm

class EmailService {

    def sessionFactory

    void fetchUnreadEmails(Team team) {

        if (!team.onSupport) {
            log.info("The team ${team} currently has nobody on support, so not sending SMS.")
            return
        }

        Store store
        try {
            store = getSessionStore(team.email, team.password)
        } catch (AuthenticationFailedException e) {
            log.error("Authentication failed while trying to get email for team ${team}", e)
            team.passwordExpired = true
            team.save(flush: true)
            return
        }
        Folder inbox = store.getFolder('Inbox')
        inbox.open(Folder.READ_WRITE)
        FlagTerm flagTerm = new FlagTerm(new Flags(Flags.Flag.SEEN), false)
        List<Message> messages = inbox.search(flagTerm)

        for(Message message in messages) {
            MimeMultipart multipart = message.content
            for (int i = 0; i < multipart.count; i++) {
                BodyPart messagePart = multipart.getBodyPart(i)
                if(messagePart.contentType.toLowerCase().startsWith('text/plain')) {
                    Sms sms = new Sms(message: messagePart.content, recipient: team.onSupport)
                    if(!sms.save(flush: false)) {
                        log.error("Error saving SMS from mail: ${sms.message}")
                    }
                    break
                }
            }
            message.setFlag(Flags.Flag.SEEN, true)
        }
        sessionFactory.currentSession.flush()
    }

    Store getSessionStore(String email, String password) {
        Properties props = System.properties
        props.setProperty('mail.store.protocol', 'imaps')

        Session session = Session.getDefaultInstance(props)
        Store store = session.getStore('imaps')
        store.connect('imap.gmail.com', email, password)
        return store
    }
}
