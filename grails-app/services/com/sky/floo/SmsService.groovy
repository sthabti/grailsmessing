package com.sky.floo

import org.marre.SmsSender

class SmsService {

    def grailsApplication
    def sessionFactory

    def sendBatchMessages() {
        def messages = Sms.listOrderByDateCreated(max: grailsApplication.config.messages.batchCount)
        for (sms in messages) {
            sendSms(sms)
            sms.delete(flush: false)
        }
        sessionFactory.currentSession.flush()
    }

    def sendSms(Sms sms) {
        withSmsSender { SmsSender smsSender ->
            smsSender.sendTextSms(sms.message, sms.recipient.phoneNumber)
        }
    }

    private void withSmsSender(Closure closure) {
        def config = grailsApplication.config.clickatell
        SmsSender smsSender = SmsSender.getClickatellSender(config.username, config.password, config.apiid)
        smsSender.connect()
        try {
            closure.call(smsSender)
        } catch (Exception e) {
            log.error("An error occurred while sending SMS", e)
        } finally {
            smsSender.disconnect()
        }
    }
}
