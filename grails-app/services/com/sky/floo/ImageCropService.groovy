package com.sky.floo

import java.awt.Rectangle
import java.awt.image.BufferedImage

class ImageCropService {

    def crop(BufferedImage image, int x, int y, int width, int height) {

        BufferedImage dest = image.getSubimage(x, y, width, height)

        return dest

    }
}
