package com.sky.floo

import grails.plugins.springsecurity.Secured

@Secured(['ROLE_ADMIN'])
class TeamController {

    def springSecurityService

    static allowedMethods = [
        index: 'GET',
        save: 'POST',
        delete: 'POST'
    ]

    def index() {
        render view: 'index', model: [teams: Team.listOrderByName().minus(springSecurityService.currentUser)]
    }

    def save(String name, String email, String password) {
        Role userRole = Role.findByAuthority('ROLE_USER')
        Team team = new Team(name: name, email: email, password: password, enabled: true)
        if (team.save()) {
            TeamRole.create(team, userRole, true)
            flash.success = "The team \"${name}\" was created successfully."
            redirect action: 'index'
        } else {
            render view: 'index', model: [team: team, teams: Team.listOrderByName()]
        }
    }

    def delete(Long id) {
        Team team = Team.findById(id)
        if (team && team.delete()) {
            flash.success = "The team \"${team.name}\" was deleted successfully."
        } else {
            flash.error = "The team you tried to delete does not exist."
        }
        redirect action: 'index'
    }
}
