package com.sky.floo

import grails.plugins.springsecurity.Secured

@Secured(['ROLE_USER'])
class MemberController {

    def springSecurityService

    static allowedMethods = [
        index: 'GET',
        create: 'GET',
        save: 'POST',
        edit: 'GET',
        update: 'POST',
        delete: 'POST'
    ]

    def beforeInterceptor = [action: this.&checkTeam]

    private checkTeam() {
        if (params.teamId != springSecurityService.currentUser?.id?.toString()) {
            render view: '/login/denied'
            return false
        }
    }

    def index(Long teamId) {
        Team team = Team.findById(teamId)
        if (team) {
            render view: 'index', model: [team: team, members: Member.findAllByTeam(team, [sort: 'dateCreated', order: 'asc'])]
        } else {
            flash.error = 'The team you are looking for does not exist or is not accessible by you.'
            redirect url: '/'
        }
    }

    def create(Long teamId) {
        Team team = Team.findById(teamId)
        if (team) {
            render template: 'form', model: [team: team]
        } else {
            render view: '/error'
        }
    }

    def save(String name, String phoneNumber, String avatarUrl, Long teamId) {
        Team team = Team.findById(teamId)
        if (team) {
            Member member = new Member(name: name, phoneNumber: phoneNumber, team: team)
            if (member.save()) {
                flash.success = "${member.name} is now added to the team."
                redirect controller: 'team', action: 'members', id: teamId
            } else {
                render view: '/member/index', model: [team: team, members: team.members, member: member]
            }
        } else {
            flash.error = 'The team you are trying to add a member does not exist or is not accessible by you.'
            redirect url: '/'
        }
    }

    def edit(Long id, Long teamId) {
        Member member = Member.findById(id)
        Team team = Team.findById(teamId)
        if (member && team && member.team == team) {
            render template: 'form', model: [team: team, member: member]
        } else {
            render view: '/error'
        }
    }

    def update(Long id, String name, String phoneNumber, String avatarUrl, Long teamId) {
        Team team = Team.findById(teamId)
        if (team) {
            Member member = new Member(name: name, phoneNumber: phoneNumber, team: team)
            if (member.save()) {
                flash.success = "${member.name} is now added to the team."
                redirect action: 'members', id: teamId
            } else {
                render view: '/member/index', model: [team: team, members: team.members, member: member]
            }
        } else {
            flash.error = 'The team you are trying to add a member does not exist or is not accessible by you.'
            redirect url: '/'
        }
    }

    def delete(Long id, Long teamId) {
        Member member = Member.findById(id)
        Team team = Team.findById(teamId)
        if (member && team && member.team == team && member.delete()) {
            flash.success = "${member.name} was successfully deleted from the team."
        } else {
            flash.error = "The member you tried to delete was not accessible."
        }
        redirect controller: 'team', action: 'members', id: teamId
    }
}
