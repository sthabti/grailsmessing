package com.sky.floo

import javax.imageio.ImageIO
import java.awt.image.BufferedImage

class CropperController {

    def index() {
        def service = new ImageCropService();


        URL url = new URL("http://www.sky.com/hp/image/65e574ac-ab56-4888-9b2d-169daf2762e2/desktopFront.jpg");
        BufferedImage  image = ImageIO.read(url);

        def x = params.int('x', 100)
        def y = params.int('y', 100)
        def width = params.int('width', 100)
        def height = params.int('height', 100)


        BufferedImage crop = service.crop(image, x, y, width, height)
        ImageIO.write(crop, "png", new File("/Users/thabtis/git/G/web-app/images/sabeur.png"));

        render view: 'index', model: [image: url]
    }


    def show() {

       render view: 'show', model: [outImage: "sabeur.png"]
    }
}
