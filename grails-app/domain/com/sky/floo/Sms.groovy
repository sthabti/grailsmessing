package com.sky.floo

import org.joda.time.DateTime

class Sms {

    String message
    Member recipient

    DateTime dateCreated
    DateTime lastUpdated

    static constraints = {
        message blank: false
    }
}
