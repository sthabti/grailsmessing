package com.sky.floo
import org.joda.time.DateTime

class Team {

    transient springSecurityService

    String name
    String email
    String password
    Member onSupport
    boolean messagesActive = true
    Long sentMessageCount = 0

    boolean enabled
    boolean accountExpired
    boolean accountLocked
    boolean passwordExpired

    DateTime dateCreated
    DateTime lastUpdated

    static constraints = {
        name blank: false
        email blank: false, unique: true, email: true
        onSupport nullable: true
    }

    static hasMany = [members: Member]

    static mapping = {
        password column: '`password`'
    }

    Set<Role> getAuthorities() {
        TeamRole.findAllByTeam(this).collect { it.role } as Set
    }

    def beforeInsert() {
        encodePassword()
    }

    def beforeUpdate() {
        if (isDirty('password')) {
            encodePassword()
        }
    }

    protected void encodePassword() {
        password = springSecurityService.encodePassword(password)
    }
}
