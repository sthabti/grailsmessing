package com.sky.floo

import org.joda.time.DateTime

class Member {

    String name
    String phoneNumber
    String avatarUrl

    DateTime dateCreated
    DateTime lastUpdated

    static constraints = {
        name blank: false
        phoneNumber blank: false, matches: '44[0-9]{10}'
        avatarUrl nullable: true, blank: true, url: true
    }

    static belongsTo = [team: Team]
}
