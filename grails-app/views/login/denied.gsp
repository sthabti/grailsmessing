<head>
<meta name='layout' content='main' />
<title><g:message code="springSecurity.denied.title" /></title>
</head>
<body>
	<div class="alert alert-danger">
        <g:message code="springSecurity.denied.message" />
        <g:link controller="login">Back to home page</g:link>
    </div>
</body>
