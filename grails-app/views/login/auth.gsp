<html>
<head>
	<meta name='layout' content='main'/>
	<title>Login</title>
</head>
<body>
<form action="${postUrl}" method="POST" class="well form-horizontal col-lg-6">
    <fieldset>
        <legend>Enter Your Credentials</legend>

        <div class="form-group">
            <label for="username" class="col-lg-2 control-label">Email:</label>
            <div class="col-lg-10">
                <g:field type="email" name="j_username" id="username" class="form-control" required="required"
                             placeholder="username@gmail.com" autofocus="autofocus" />
            </div>
        </div>

        <div class="form-group">
            <label for="password" class="col-lg-2 control-label">Password</label>
            <div class="col-lg-10">
                <g:field type="password" name="j_password" id="password" class="form-control col-lg-10" required="required"
                         placeholder="Password of the above email account" />
            </div>
        </div>

        <div class="form-group">
            <div class="col-lg-offset-2 col-lg-10">
                <button class="btn btn-primary" type="submit">Login</button>
            </div>
        </div>

    </fieldset>
</form>
</body>
</html>
