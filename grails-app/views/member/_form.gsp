<div class="form-group">
    <label for="name">Name</label>
    <g:textField name="name" value="${member?.name}" class="form-control" required="required"
                 placeholder="Example: Tahir Shah" />
    <g:hasErrors bean="${member}" field="name">
        <g:eachError bean="${member}" field="name">
            <div class="text-danger">
                <g:message error="${it}" />
            </div>
        </g:eachError>
    </g:hasErrors>
</div>

<div class="form-group">
    <label for="name">Phone Number</label>
    <g:field type="tel" name="phoneNumber" value="${member?.phoneNumber}" class="form-control" required="required"
                 placeholder="Example: 447567567567" />
    <g:hasErrors bean="${member}" field="phoneNumber">
        <g:eachError bean="${member}" field="phoneNumber">
            <div class="text-danger">
                <g:message error="${it}" />
            </div>
        </g:eachError>
    </g:hasErrors>
</div>

<div class="form-group">
    <label for="avatarUrl">Avatar URL</label>
    <g:field type="url" name="avatarUrl" value="${member?.avatarUrl}" class="form-control"
             placeholder="Example: http://placekitten.com/200/200" />
    <g:hasErrors bean="${member}" field="avatarUrl">
        <g:eachError bean="${member}" field="avatarUrl">
            <div class="text-danger">
                <g:message error="${it}" />
            </div>
        </g:eachError>
    </g:hasErrors>
</div>

