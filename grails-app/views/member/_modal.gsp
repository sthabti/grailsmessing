<div class="modal fade" id="member-modal-${member?.id ?: 'new'}">
    <div class="modal-dialog">
        <div class="modal-content">
            <g:form controller="member" action="${member?.id ? 'update' : 'save'}"
                    params="${member?.id ? [id: member.id, teamId: team.id] : [teamId: team.id]}">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h2 class="modal-title">
                        <g:if test="${member?.id}">
                            Edit Member Details
                        </g:if>
                        <g:else>
                            Add New Member
                        </g:else>
                    </h2>
                </div>
                <div class="modal-body">
                    <g:render template="/member/form" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-success">Add Member</button>
                </div>
            </g:form>
        </div>
    </div>
</div>
