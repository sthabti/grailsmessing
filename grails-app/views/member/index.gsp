<html>
<head>
    <meta name="layout" content="main" />
    <title>${team.name}</title>
</head>
<body>

    <div class="row">
        <g:each in="${members}" var="mmbr">
            <g:render template="/member/modal" model="[member: mmbr]" />
            <a data-toggle="modal" href="#member-modal-${mmbr.id}" class="col-6 col-sm-4 col-lg-3">
                <div class="thumbnail">
                    <img src="http://placekitten.com/300/200" alt="" style="width: 100%" />
                    <div class="caption">
                        <strong>${mmbr.name}</strong>
                        <p>${mmbr.phoneNumber}</p>
                    </div>
                </div>
            </a>
        </g:each>
        <g:render template="/member/modal" />
        <a data-toggle="modal" href="#member-modal-new" class="col-6 col-sm-4 col-lg-3">
            <div class="thumbnail" style="padding-bottom: 2.4em;">
                <img src="http://placekitten.com/300/200" alt="" style="width: 100%" />
                <div class="caption" style="text-align: center;">
                    <strong>Add New Member</strong>
                </div>
            </div>
        </a>
    </div>
</body>
</html>
