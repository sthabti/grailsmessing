<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>Floo | <g:layoutTitle /></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon">
        <r:require modules="bootstrap, application" />
		<g:layoutHead/>
		<r:layoutResources />
	</head>
	<body>
        <nav class="navbar navbar-fixed-top navbar-inverse">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <g:link controller="login" class="navbar-brand">Floo</g:link>
            <sec:ifLoggedIn>
                <ul class="nav navbar-nav pull-right nav-collapse collapse navbar-responsive-collapse">
                    <li>
                        <g:link controller="logout">Logout</g:link>
                    </li>
                </ul>
            </sec:ifLoggedIn>
        </nav>
        <g:if test="${flash.message}">
            <div class="alert alert-info">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                ${flash.message}
            </div>
        </g:if>
        <g:if test="${flash.success}">
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                ${flash.success}
            </div>
        </g:if>
        <g:if test="${flash.error}">
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                ${flash.error}
            </div>
        </g:if>
        <header class="container page-header">
            <h1><g:layoutTitle /></h1>
        </header>
        <div class="container">
            <g:layoutBody/>
        </div>
		<r:layoutResources />
	</body>
</html>
