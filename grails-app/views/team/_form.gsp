<g:form controller="team" action="save" class="well form-horizontal">
    <fieldset>
        <legend>Create New Team</legend>

        <div class="form-group">
            <label for="name" class="col-lg-2 control-label">Team Name</label>
            <div class="col-lg-10">
                <g:textField name="name" value="${team?.name}" class="form-control" required="required"
                             placeholder="Example: Homepage Team" autofocus="autofocus" />
                <g:hasErrors bean="${team}" field="name">
                    <g:eachError bean="${team}" field="name">
                        <div class="text-danger">
                            <g:message error="${it}" />
                        </div>
                    </g:eachError>
                </g:hasErrors>
            </div>
        </div>

        <div class="form-group">
            <label for="email" class="col-lg-2 control-label">Alerts Email</label>
            <div class="col-lg-10">
                <g:field type="email" name="email" value="${team?.email}" class="form-control col-lg-10" required="required"
                         placeholder="homepagealerts@gmail.com" />
                <g:hasErrors bean="${team}" field="email">
                    <g:eachError bean="${team}" field="email">
                        <div class="text-danger">
                            <g:message error="${it}" />
                        </div>
                    </g:eachError>
                </g:hasErrors>
            </div>
        </div>

        <div class="form-group">
            <label for="password" class="col-lg-2 control-label">Password</label>
            <div class="col-lg-10">
                <g:field type="password" name="password" value="" class="form-control col-lg-10" required="required"
                         placeholder="Password of the above email account" />
                <g:hasErrors bean="${team}" field="password">
                    <g:eachError bean="${team}" field="password">
                        <div class="text-danger">
                            <g:message error="${it}" />
                        </div>
                    </g:eachError>
                </g:hasErrors>
            </div>
        </div>

        <div class="form-group">
            <div class="col-lg-offset-2 col-lg-10">
                <button class="btn btn-success" type="submit">Create</button>
            </div>
        </div>

    </fieldset>
</g:form>
<hr />
