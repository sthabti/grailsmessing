<html>
<head>
    <title>Manage Teams</title>
    <meta name="layout" content="main" />
</head>
<body>
    <g:render template="form" />
    <h3>Existing Teams</h3>
    <g:render template="list" />
</body>
</html>