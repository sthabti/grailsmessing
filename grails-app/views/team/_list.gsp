<table class="table table-bordered table-hover">
    <thead>
    <tr>
        <th class="col-6">Team Name</th>
        <th class="col-3" style="text-align: center">Email</th>
        <th class="col-3" style="text-align: center">On Support</th>
    </tr>
    </thead>
    <tbody>
        <g:if test="${teams}">
            <g:each in="${teams}" var="team">
                <tr>
                    <td>
                        ${team.name}
                        <span class="badge pull-right">
                            ${team.members.size()}
                            <inf:pluralize count="${team.members.size()}">member</inf:pluralize>
                        </span>
                    </td>
                    <td style="text-align: center">
                        <a href="mailto:${team.email}">${team.email}</a>
                    </td>
                    <td  style="text-align: center">
                        <g:if test="${team.onSupport}">
                            <a href="tel:${team.onSupport.phoneNumber}">${team.onSupport.name}</a>
                        </g:if>
                        <g:else>None</g:else>
                    </td>
                </tr>
            </g:each>
        </g:if>
        <g:else>
            <tr>
                <td colspan="3">There are currently no teams.</td>
            </tr>
        </g:else>
    </tbody>
</table>
