<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
  <title></title>
    <meta name='layout' content='main'/>
</head>
<body>
<h1>Hello</h1>
<g:form action="index" >
    <fieldset>

        <!-- Form Name -->
        <legend>Image Cropper</legend>

        <!-- Text input-->
        <div class="control-group">
            <label class="control-label" for="x">X Axis</label>
            <div class="controls">
                <input id="x" name="x" type="text" placeholder="" class="input-large" required="">

            </div>
        </div>

        <!-- Text input-->
        <div class="control-group">
            <label class="control-label" for="y">Y axis</label>
            <div class="controls">
                <input id="y" name="y" type="text" placeholder="" class="input-large" required="">

            </div>
        </div>

        <!-- Text input-->
        <div class="control-group">
            <label class="control-label" for="width">Width</label>
            <div class="controls">
                <input id="width" name="width" type="text" placeholder="" class="input-large">

            </div>
        </div>

        <!-- Text input-->
        <div class="control-group">
            <label class="control-label" for="height">Height</label>
            <div class="controls">
                <input id="height" name="height" type="text" placeholder="" class="input-large">

            </div>
        </div>

        <!-- Button -->
        <div class="control-group">
            <label class="control-label" for="singlebutton">Single Button</label>
            <div class="controls">
                <button id="singlebutton" name="singlebutton" class="btn btn-primary">Button</button>
            </div>
        </div>

    </fieldset>
</g:form>

<h2>Original</h2>
<img id="sabeur"  src="${image}" alt=""/>
<script type="text/javascript">$(document).ready(function() {
    $('#sabeur').click(function(e) {
        var offset = $(this).offset();
        console.log(e.clientX - offset.left);
        console.log(e.clientY - offset.top);
    });
});</script>
</body>

</html>